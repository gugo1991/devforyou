export const state = () => ({
    list: [
        {
            name: "Gurgen",
            id: 1
        },
        {
            name: "Hrach",
            id: 2
        },
        {
            name: "Tigran",
            id: 3
        },
    ]
})

export const mutations = {
    toggle(state, todo) {
        todo.done = !todo.done
    }
}
export const getters = {
    list(state) {
        return state.list
    }
}
